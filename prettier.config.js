module.exports = {
    tabWidth: 4,
    printWidth: 100,
    semi: true,
    trailingComma: 'es5',
    bracketSpacing: true,
    singleQuote: true,
    endOfLine: 'lf',
    overrides: [
        {
            files: '*.json',
            options: {
                tabWidth: 2,
                singleQuote: false,
            },
        },
    ],
};
