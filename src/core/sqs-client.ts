import { SQS } from 'aws-sdk';

const queueUrl = process.env.QUEUE_URL as string;
const client = new SQS({});

// todo: add return type
export async function sendMessageToQueue(message: string) {
    const params = {
        QueueUrl: queueUrl,
        MessageBody: message,
    };

    return client.sendMessage(params).promise();
}
