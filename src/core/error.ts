import type { APIGatewayProxyEvent } from 'aws-lambda';

import { ResponseCodes } from './consts';
import { logger } from './logger';

type KeyValue = Record<string, unknown>;

export class AppError extends Error {
    constructor(message?: string, public context: KeyValue = {}) {
        super(message);
    }
}

export class HttpError extends AppError {
    constructor(message: string, public httpStatus: ResponseCodes, context?: KeyValue) {
        super(message, context);
    }
}

export class InvalidJsonBodyError extends HttpError {
    constructor(message: string) {
        super(message, ResponseCodes.badRequest);
    }
}

export class NotFoundError extends HttpError {
    constructor(message: string, context?: KeyValue) {
        super(message, ResponseCodes.notFound, context);
    }
}

export class BadRequestError extends HttpError {
    constructor(message: string, context?: KeyValue) {
        super(message, ResponseCodes.badRequest, context);
    }
}

export class UnprocessableError extends HttpError {
    constructor(message: string, context?: KeyValue) {
        super(message, ResponseCodes.unprocessable, context);
    }
}

function catchApplicationErrors(error: Error) {
    if (error instanceof AppError) {
        const statusCode = (error as HttpError).httpStatus || ResponseCodes.error;
        const message = error.message || 'Not provided';
        const context = error.context ?? {};

        if (error instanceof InvalidJsonBodyError) {
            logger.info({ error }, 'Invalid JSON body error was caught by error handler');
            return {
                statusCode,
                body: JSON.stringify({
                    message: 'The body of your request is not valid JSON',
                }),
            };
        }

        if (error instanceof BadRequestError) {
            logger.info({ error }, 'Bad request error was caught by error handler');
            return {
                statusCode,
                body: JSON.stringify(context),
            };
        }

        logger.info({ error }, 'AppError was caught by error handler');
        return {
            statusCode,
            body: JSON.stringify({ message, context }),
        };
    }

    // todo: alert the monitoring service
    logger.fatal({ error }, 'Unknown error was caught by error handler');
    return {
        statusCode: ResponseCodes.error,
        body: JSON.stringify({
            message: 'Unknown error has occurred',
        }),
    };
}

type Callback = (
    event: APIGatewayProxyEvent
) => Promise<{ body: string; statusCode: ResponseCodes }>;

// This error handler is responsible for handling application wide errors.
// So, the application logic doesn't have to individually handle errors, instead
// it can throw a proper error and let this handler handle it.
export function withErrorHandler(callback: Callback) {
    return (event: APIGatewayProxyEvent) =>
        Promise.resolve(callback(event)).catch(catchApplicationErrors);
}
