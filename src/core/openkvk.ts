import fetch, { RequestInit } from 'node-fetch';

import { HttpError } from './error';

const baseUrl = process.env.OPENKVK_API_URL as string;
const apiKey = process.env.OPENKVK_API_KEY as string;

const reqConfig: RequestInit = {
    headers: {
        'ovio-api-key': apiKey,
    },
};

// todo: define an interface for the company response
export async function getCompanyById(id: string): Promise<Record<string, unknown>> {
    const res = await fetch(`${baseUrl}/${id}`, reqConfig);

    if (!res.ok) {
        throw new HttpError('An error occurred while fetching the company data', res.status, {
            statusText: res.statusText,
            url: res.url,
        });
    }

    return res.json();
}
