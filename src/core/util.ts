import { InvalidJsonBodyError } from './error';

export function jsonParser(data: string) {
    try {
        return JSON.parse(data);
    } catch (error) {
        throw new InvalidJsonBodyError('Invalid JSON body error was caught');
    }
}
