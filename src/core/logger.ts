import pino from 'pino';

const logLevel = process.env.LOG_LEVEL;
const defaultLogLevel = 'info';

export const logger = pino({
    level: logLevel ?? defaultLogLevel,
    serializers: {
        // In addition to `err` key, Error can be passed via `error` as well
        error: pino.stdSerializers.err,
        req: pino.stdSerializers.req,
        res: pino.stdSerializers.res,
    },
});
