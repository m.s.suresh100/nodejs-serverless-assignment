import type { SQSEvent } from 'aws-lambda';

import { markLoansAsDisbursed } from './service';
import { logger } from '../core';

async function disbursementJobHandler(event: SQSEvent) {
    const [record] = event.Records;
    // body { loanId: 'xx' }
    const message = JSON.parse(record.body);

    logger.info({ record }, 'Processing disbursement request');

    // No need to catch any error here. Queue will retry failed messages upto 5 times.
    // Messages that still fail to be processed are stored in the SQS dead letter queue
    await markLoansAsDisbursed(message.loanId);
}

export const handler = disbursementJobHandler;
