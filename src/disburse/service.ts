import { updateLoanStatus } from '../loan/repository';
import { LOAN_STATUS } from '../loan/domain';
import { logger } from '../core';

export async function markLoansAsDisbursed(loanId: string) {
    await updateLoanStatus(loanId, LOAN_STATUS.disbursed);
    logger.info({ loanId }, 'Loan has successfully disbursed');
}
