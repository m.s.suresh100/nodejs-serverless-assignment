import { DBClient } from '../core/db-client';
import { Loan, LOAN_STATUS } from './domain';

const tableName = process.env.DYNAMODB_TABLE as string;

export async function create(data: Loan) {
    const params = {
        TableName: tableName,
        Item: data,
    };

    await DBClient.put(params).promise();
}

export async function getById(loanId: string): Promise<Loan | undefined> {
    const params = {
        TableName: tableName,
        Key: { id: loanId },
    };

    const result = await DBClient.get(params).promise();

    if (!result.Item) return;

    return result.Item as Loan;
}

export async function deleteLoanById(loanId: string): Promise<Loan | undefined> {
    const params = {
        TableName: tableName,
        Key: { id: loanId },
        UpdateExpression: 'SET isDeleted = :d, updatedAt = :u',
        ExpressionAttributeValues: {
            ':d': true,
            ':u': Date.now().toString(),
        },
        ReturnValues: 'ALL_NEW',
    };

    const result = await DBClient.update(params).promise();

    // Updated item can be undefined
    if (!result.Attributes) return;

    return result.Attributes as Loan;
}

export async function fetchAllLoans(): Promise<Loan[]> {
    const params = {
        TableName: tableName,
        FilterExpression: 'isDeleted = :d',
        ExpressionAttributeValues: {
            ':d': false,
        },
    };

    const result = await DBClient.scan(params).promise();

    if (!result.Items) return [];

    return result.Items as Loan[];
}

export async function updateLoanStatus(loanId: string, loanStatus: LOAN_STATUS) {
    const params = {
        TableName: tableName,
        Key: { id: loanId },
        // `status` is a reserved keyword in Dynamodb.
        // So, can't directly use it in the update expression
        UpdateExpression: 'SET #statusKeyName = :s, updatedAt = :u',
        ExpressionAttributeNames: {
            '#statusKeyName': 'status',
        },
        ExpressionAttributeValues: {
            ':s': loanStatus,
            ':u': Date.now().toString(),
        },
        ReturnValues: 'ALL_NEW',
    };

    await DBClient.update(params).promise();
}
