import { nanoid } from 'nanoid';

export enum LOAN_STATUS {
    offered = 'offered',
    processing = 'processing',
    disbursed = 'disbursed',
}

// fixme: define proper types for the company
type Company = Record<string, unknown>;

export interface Loan {
    id: string;
    amount: number;
    status: LOAN_STATUS;
    company: Company;
    createdAt: string;
    updatedAt: string;
    isDeleted: boolean;
}

export function getLoanData(amount: number, companyData: Company): Loan {
    const timestamp = Date.now().toString();

    return {
        id: nanoid(),
        amount,
        status: LOAN_STATUS.offered,
        company: companyData,
        createdAt: timestamp,
        updatedAt: timestamp,
        isDeleted: false,
    };
}

export function hasLoanDisbursed(status: LOAN_STATUS): boolean {
    return status === LOAN_STATUS.disbursed;
}

export function isLoanInProcess(status: LOAN_STATUS): boolean {
    return status === LOAN_STATUS.processing;
}
