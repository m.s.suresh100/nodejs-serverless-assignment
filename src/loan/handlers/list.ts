import type { APIGatewayProxyResult } from 'aws-lambda';

import { getAllLoans } from '../service';
import { ResponseCodes, withErrorHandler } from '../../core';

async function ListLoansHandler(): Promise<APIGatewayProxyResult> {
    const loans = await getAllLoans();

    return {
        statusCode: ResponseCodes.ok,
        body: JSON.stringify(loans),
    };
}

export const handler = withErrorHandler(ListLoansHandler);
