import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

import { markLoanAsDeleted } from '../service';
import { ResponseCodes, withErrorHandler } from '../../core';

async function deleteLoanHandler(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const loanId = event.pathParameters?.id ?? '';
    const deletedLoan = await markLoanAsDeleted(loanId);

    return {
        statusCode: ResponseCodes.ok,
        body: JSON.stringify(deletedLoan),
    };
}

export const handler = withErrorHandler(deleteLoanHandler);
