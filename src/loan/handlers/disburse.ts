import type { APIGatewayProxyEvent, APIGatewayProxyResult } from 'aws-lambda';

import { disburseLoan } from '../service';
import { ResponseCodes, withErrorHandler } from '../../core';

async function disburseLoanHandler(event: APIGatewayProxyEvent): Promise<APIGatewayProxyResult> {
    const loanId = event.pathParameters?.id ?? '';
    const result = await disburseLoan(loanId);

    return {
        statusCode: ResponseCodes.accepted,
        body: JSON.stringify(result),
    };
}

export const handler = withErrorHandler(disburseLoanHandler);
