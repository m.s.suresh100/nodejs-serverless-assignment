import 'jest-extended';
import nock from 'nock';

import { handler } from '../apply';
import { ResponseCodes } from '../../../core';
import { LOAN_STATUS } from '../../domain';
import * as repository from '../../repository';

let mockServer: nock.Scope;
beforeEach(() => {
    mockServer = nock(process.env.OPENKVK_API_URL as string);
});

afterAll(() => {
    nock.cleanAll();
});

describe('Apply for a loan', () => {
    it('should return 400 status code if the request body is malformed', async () => {
        const event = {
            body: "{'amount': 40,}",
        };
        // handler expects APIGatewayProxyEvent type
        const res = await handler(event as any);

        expect(res.statusCode).toEqual(ResponseCodes.badRequest);

        const body = JSON.parse(res.body);
        expect(body).toEqual({
            message: 'The body of your request is not valid JSON',
        });
    });

    describe('should validate the request body', () => {
        it('the amount and companyId are required fields', async () => {
            const event = {
                body: JSON.stringify({}),
            };
            const res = await handler(event as any);

            expect(res.statusCode).toEqual(ResponseCodes.badRequest);

            const body = JSON.parse(res.body);
            expect(body).toMatchSnapshot();
        });

        it('the amount field must be a positive number', async () => {
            const event = {
                body: JSON.stringify({
                    amount: 'amount',
                }),
            };
            const res = await handler(event as any);

            expect(res.statusCode).toEqual(ResponseCodes.badRequest);

            const body = JSON.parse(res.body);
            expect(body).toMatchSnapshot();
        });
    });

    it('should return 422 status code if the company is not active in KVK', async () => {
        const companyId = 'inactive-company';
        mockServer.get(`/${companyId}`).reply(200, {
            actief: false,
        });

        const event = {
            body: JSON.stringify({
                amount: 100,
                companyId,
            }),
        };
        const res = await handler(event as any);

        expect(res.statusCode).toEqual(ResponseCodes.unprocessable);

        const body = JSON.parse(res.body);
        expect(body).toMatchSnapshot();
    });

    it('should return the created loan with the status code 201', async () => {
        jest.spyOn(repository, 'create').mockImplementationOnce(() => Promise.resolve());

        const companyId = 'active-company';
        const companyMock = {
            name: 'abc ltd',
            actief: true,
        };
        mockServer.get(`/${companyId}`).reply(200, companyMock);

        const event = {
            body: JSON.stringify({
                amount: 100,
                companyId,
            }),
        };
        const res = await handler(event as any);

        expect(res.statusCode).toEqual(ResponseCodes.created);

        const body = JSON.parse(res.body);
        expect(body).toEqual({
            id: expect.toBeString(),
            amount: 100,
            company: companyMock,
            status: LOAN_STATUS.offered,
            createdAt: expect.toBeString(),
            updatedAt: expect.toBeString(),
            isDeleted: false,
        });
    });
});
