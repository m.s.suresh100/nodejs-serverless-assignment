import 'jest-extended';
import { getLoanData, LOAN_STATUS, hasLoanDisbursed, isLoanInProcess } from '../domain';

describe('getLoanData', () => {
    it('should return the initial loan data', () => {
        const amount = 300;
        const companyData = {
            name: 'foo',
        };

        const loanData = getLoanData(amount, companyData);

        expect(loanData).toMatchObject({
            amount,
            company: companyData,
            isDeleted: false,
            status: LOAN_STATUS.offered,
        });

        // nano id has 21 characters
        expect(loanData.id).toHaveLength(21);
        expect(new Date(Number(loanData.createdAt))).toBeDate();
        expect(new Date(Number(loanData.updatedAt))).toBeDate();
    });
});

describe('hasLoanDisbursed', () => {
    it(`should return true if the loan status is ${LOAN_STATUS.disbursed}`, () => {
        expect(hasLoanDisbursed(LOAN_STATUS.disbursed)).toBeTrue();
    });

    it(`should return false when the status is not ${LOAN_STATUS.disbursed}`, () => {
        expect(hasLoanDisbursed(LOAN_STATUS.offered)).toBeFalse();
    });
});

describe('isLoanInProcess', () => {
    it(`should return true if the loan status is ${LOAN_STATUS.processing}`, () => {
        expect(isLoanInProcess(LOAN_STATUS.processing)).toBeTrue();
    });

    it(`should return false when the status is not ${LOAN_STATUS.processing}`, () => {
        expect(isLoanInProcess(LOAN_STATUS.disbursed)).toBeFalse();
    });
});
