const { nodeExternalsPlugin } = require('esbuild-node-externals');

// To mark all the dependencies in package.json as external
// Packages that are marked as external and exist in the package.json's dependencies,
// will be installed and included with your build under node_modules.
module.exports = [nodeExternalsPlugin()];
