service: nodejs-serverless-assignment
frameworkVersion: '3'

provider:
  name: aws
  runtime: nodejs14.x
  region: ap-south-1
  environment: ${file(./config/env.${sls:stage}.yml)}
  iam:
    role:
      statements:
        # permission to write/retrieve data from/to dynamodb
        - Effect: Allow
          Action:
            - dynamodb:Scan
            - dynamodb:GetItem
            - dynamodb:PutItem
            - dynamodb:UpdateItem
          Resource:
            - Fn::GetAtt: [loansTable, Arn]

        # permission to send message to sqs
        - Effect: Allow
          Action:
            - sqs:SendMessage
          Resource:
            - Fn::GetAtt: [disburseQueue, Arn]

package:
  individually: true

plugins:
  - serverless-esbuild
  - serverless-dynamodb-local
  - serverless-offline

custom:
  esbuild:
    minify: true
    plugins: ./plugins.js

  dynamodb:
    stages:
      - dev # run this only on "dev" stage
    start:
      migrate: true
      noStart: true
    migration:
      dir: migrations

functions:
  applyLoan:
    handler: src/loan/handlers/apply.handler
    events:
      - http:
          path: /loans
          method: post

  deleteLoan:
    handler: src/loan/handlers/delete.handler
    events:
      - http:
          path: /loans/{id}
          method: delete

  listLoans:
    handler: src/loan/handlers/list.handler
    events:
      - http:
          path: /loans
          method: get

  disburseLoan:
    handler: src/loan/handlers/disburse.handler
    environment:
      QUEUE_URL: { Ref: disburseQueue }
    events:
      - http:
          path: /loans/{id}/disburse
          method: post

  disburseLoanWorker:
    handler: src/disburse/worker.handler
    events:
      - sqs:
          batchSize: 1 # just fetch one event at a time
          arn:
            Fn::GetAtt: [disburseQueue, Arn]

resources:
  Resources:
    loansTable:
      Type: AWS::DynamoDB::Table
      Properties:
        TableName: ${self:provider.environment.DYNAMODB_TABLE}
        AttributeDefinitions:
          - AttributeName: id
            AttributeType: S
        KeySchema:
          - AttributeName: id
            KeyType: HASH
        BillingMode: PAY_PER_REQUEST

    disburseQueue:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: disburseQueue
        VisibilityTimeout: 20
        DelaySeconds: 3 # wait 5 seconds before dispatching events (batching)
        MessageRetentionPeriod: 120 # 2 minutes
        RedrivePolicy:
          deadLetterTargetArn:
            Fn::GetAtt: [disburseDLQ, Arn]
          maxReceiveCount: 5 # number of times a message is delivered to the source queue before being moved to the dead-letter queue.

    disburseDLQ:
      Type: AWS::SQS::Queue
      Properties:
        QueueName: disburseDeadLetterQueue
