const matchers = require('jest-extended');

expect.extend(matchers);

process.env.OPENKVK_API_URL = 'https://api.overheid.io/openkvk';

// mock pino logger
jest.mock('pino', () => {
    function pino() {
        return {
            info: jest.fn(),
            fatal: jest.fn(),
        };
    }

    pino.stdSerializers = {
        err: jest.fn(),
        req: jest.fn(),
        res: jest.fn(),
    };

    return pino;
});
